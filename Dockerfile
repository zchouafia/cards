FROM eclipse-temurin:17-jre-alpine
RUN addgroup --system spring && adduser --system  spring --ingroup spring
USER spring:spring
ARG DEPENDENCY=target/dependency
COPY ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY ${DEPENDENCY}/META-INF /app/META-INF
COPY ${DEPENDENCY}/BOOT-INF/classes /app
EXPOSE 8082
ENTRYPOINT ["java","-cp","app:app/lib/*","com.zchouafia.cards.CardsApplication"]