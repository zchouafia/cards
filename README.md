# Cards game

## Purpose

The main purpose of this project is to demonstrate Java and Spring Boot skills for a job interview

## Recommended setup 
- Maven > 3.6.3 
- Java 17
- IntelliJ

## Run
### With the IDE
To start the project follow the next steps:
 - Add new Application Configuration
 - Choose `CardsApplication` as a main class
 - Save and run the configuration
 - Go to `localhost:8082/api/v1/cards?size=10` to get some cards

### Using docker

_Build and push a new image_ :
- `mvn clean install`
- `mkdir target/dependency`
- `cd target/dependency`
- `jar -xf ../*.jar`
- `cd ../..`
- `docker build -t zakch/cards:latest .`
- If not yet logged-in, `docker login -u "xxxxx" -p "xxxxx" docker.io`
- `docker push zakch/cards:latest`

_Run the latest version of cards api_ :
* `docker run -d -p 8082:8082 --name cards-api zakch/cards:latest`

## TODO
- Swagger doc