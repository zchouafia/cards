package com.zchouafia.cards.application.exception;

import com.zchouafia.cards.domain.model.ApiError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
public class ExceptionHandler extends ResponseEntityExceptionHandler {

    @org.springframework.web.bind.annotation.ExceptionHandler({ RuntimeException.class, NullPointerException.class })
    public ResponseEntity<ApiError> handleRuntimeException(Exception exception) {
        log.error("RuntimeException caught", exception.getCause());
        return ResponseEntity.internalServerError().body(new ApiError("Internal Server Error. Please, try again later."));
    }
}
