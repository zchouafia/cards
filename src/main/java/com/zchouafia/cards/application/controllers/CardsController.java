package com.zchouafia.cards.application.controllers;

import com.zchouafia.cards.domain.model.CardsRequest;
import com.zchouafia.cards.domain.ports.application.CardsService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/v1/cards")
public class CardsController {

    private final CardsService cardsService;

    public CardsController(CardsService cardsService) {
        this.cardsService = cardsService;
    }

    @GetMapping()
    public ResponseEntity<Object> getCards(@RequestParam("size") int size) {
        return ResponseEntity.ok().body(cardsService.getCards(size));
    }

    @PostMapping()
    public ResponseEntity<Object> sortCards(@RequestBody @Valid CardsRequest cardsRequest) {
        log.info("Sort cards request: {}", cardsRequest.toString());
        return ResponseEntity.ok().body(cardsService.sortCards(cardsRequest.getCards()));
    }
}
