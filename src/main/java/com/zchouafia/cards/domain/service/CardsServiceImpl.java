package com.zchouafia.cards.domain.service;

import com.zchouafia.cards.domain.model.Card;
import com.zchouafia.cards.domain.ports.application.CardsService;
import com.zchouafia.cards.domain.ports.infrastructure.CardsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Slf4j
@Service
public class CardsServiceImpl implements CardsService {

    private final CardsRepository cardsRepository;

    public CardsServiceImpl(CardsRepository cardsRepository) {
        this.cardsRepository = cardsRepository;
    }

    @Override
    public List<Card> getCards(int cardsSize) {
        log.info("Get {} card(s)", cardsSize);
        return cardsRepository.getCards(cardsSize);
    }

    @Override
    public List<Card> sortCards(List<Card> cards) {
        log.info("Cards to sort: {}", cards.toString());

        List<String> colorsOrder = cardsRepository.getColorsOrder();
        List<Integer> numbersOrder = cardsRepository.getNumbersOrder();

        if (colorsOrder == null || colorsOrder.isEmpty()) {
            log.error("colors order is not available");
            return Collections.emptyList();
        }

        if (numbersOrder == null || numbersOrder.isEmpty()) {
            log.error("numbers order is not available");
            return Collections.emptyList();
        }

        List<Card> sortedCards = cards.stream()
                .sorted((c1, c2) -> {
                    int number1 = c1.number().value();
                    int number2 = c2.number().value();
                    return Integer.compare(numbersOrder.indexOf(number1), numbersOrder.indexOf(number2));
                })
                .sorted((c1, c2) -> {
                    String color1 = c1.color().name();
                    String color2 = c2.color().name();
                    return Integer.compare(colorsOrder.indexOf(color1), colorsOrder.indexOf(color2));
                })
                .toList();

        log.info("Sorted cards: {}", sortedCards);
        return sortedCards;
    }
}
