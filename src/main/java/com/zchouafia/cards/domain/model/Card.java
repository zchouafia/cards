package com.zchouafia.cards.domain.model;

import java.util.Objects;

public record Card(CardColor color, CardNumber number) {
    public Card {
        Objects.requireNonNull(color);
        Objects.requireNonNull(number);
    }
}
