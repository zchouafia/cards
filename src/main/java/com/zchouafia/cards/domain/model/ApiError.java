package com.zchouafia.cards.domain.model;

public record ApiError(String message) {}
