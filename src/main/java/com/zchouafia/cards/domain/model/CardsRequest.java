package com.zchouafia.cards.domain.model;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CardsRequest {
    @NotNull
    private List<Card> cards;

    @Override
    public String toString() {
        return new StringBuilder()
                .append("{CardsRequest: cards=")
                .append(getCards())
                .append("}")
                .toString();
    }
}
