package com.zchouafia.cards.domain.model;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;

public enum CardColor {
    D("Carreaux"),
    H("Coeur"),
    P("Pique"),
    C("Trefle");

    private final String value;

    @JsonValue
    public String value() {
        return this.value;
    }

    CardColor(String value) {
        this.value = value;
    }

    public static CardColor ofValue(String value) {
        return Arrays.stream(CardColor.values())
                .filter(val -> val.value().equals(value))
                .findFirst()
                .orElse(null);
    }
}
