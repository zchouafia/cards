package com.zchouafia.cards.domain.model;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;

public enum CardNumber {
    A(1),
    N2(2),
    N3(3),
    N4(4),
    N5(5),
    N6(6),
    N7(7),
    N8(8),
    N9(9),
    N10(10),
    J(11),
    Q(12),
    K(13);

    private final int value;

    @JsonValue
    public int value() {
        return this.value;
    }

    CardNumber(int value) {
        this.value = value;
    }

    public static CardNumber ofValue(int value) {
        return Arrays.stream(CardNumber.values())
                .filter(cardNumber -> cardNumber.value() == value)
                .findFirst()
                .orElse(null);
    }
}
