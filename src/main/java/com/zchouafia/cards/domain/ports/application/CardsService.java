package com.zchouafia.cards.domain.ports.application;

import com.zchouafia.cards.domain.model.Card;

import java.util.List;

public interface CardsService {

    List<Card> getCards(int cardsSize);
    List<Card> sortCards(List<Card> cards);
}
