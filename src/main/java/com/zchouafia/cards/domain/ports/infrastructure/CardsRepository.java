package com.zchouafia.cards.domain.ports.infrastructure;

import com.zchouafia.cards.domain.model.Card;

import java.util.List;

public interface CardsRepository {

    List<String> getColorsOrder();
    List<Integer> getNumbersOrder();
    List<Card> getCards(int cardsSize);
}
