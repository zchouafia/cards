package com.zchouafia.cards.infrasctructure.adapters;

import com.zchouafia.cards.domain.model.Card;
import com.zchouafia.cards.domain.model.CardColor;
import com.zchouafia.cards.domain.model.CardNumber;
import com.zchouafia.cards.domain.ports.infrastructure.CardsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class CardsRepositoryImpl implements CardsRepository {

    @Value("#{'${cards.colors.order}'.split(';')}")
    private List<String> colorsOrder;

    @Value("#{'${cards.numbers.order}'.split(';')}")
    private List<Integer> numbersOrder;

    @Override
    public List<String> getColorsOrder() {
        return this.colorsOrder;
    }

    @Override
    public List<Integer> getNumbersOrder() {
        return this.numbersOrder;
    }

    @Override
    public List<Card> getCards(int cardsSize) {
        log.info("Generate {} random card(s)", cardsSize);
        List<CardColor> cardColors = Arrays.stream(CardColor.values()).toList();
        List<CardNumber> cardNumbers = Arrays.stream(CardNumber.values()).toList();
        List<Card> cards = new ArrayList<>();
        int i = 0;
        while(i < cardsSize) {
            int colorIndex = getRandomIndex(cardColors.size() - 1);
            int numberIndex = getRandomIndex(cardNumbers.size() - 1);
            Card card = new Card(cardColors.get(colorIndex), cardNumbers.get(numberIndex));
            // We make sure not to have duplicates
            if (!cards.contains(card)) {
                cards.add(card);
                i++;
            }
        }
        return cards;
    }

    private int getRandomIndex(int max) {
        return (int)(Math.random() * (max + 1));
    }
}
