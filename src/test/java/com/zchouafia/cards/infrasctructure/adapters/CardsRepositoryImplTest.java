package com.zchouafia.cards.infrasctructure.adapters;

import com.zchouafia.cards.domain.model.Card;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CardsRepositoryImplTest {

    private CardsRepositoryImpl sut;

    @BeforeEach
    public void setup() {
        sut = new CardsRepositoryImpl();
    }

    @Test
    public void should_return_cards() {
        //given
        //when
        List<Card> cards = sut.getCards(10);

        //then
        assertEquals(10, cards.size());
        cards.forEach(Assertions::assertNotNull);
    }

    @Test
    public void should_return_no_card() {
        //given
        //when
        List<Card> cards = sut.getCards(0);

        //then
        assertEquals(0, cards.size());
    }
}
