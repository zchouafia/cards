package com.zchouafia.cards.application.controllers;

import com.zchouafia.cards.domain.model.Card;
import com.zchouafia.cards.domain.model.CardsRequest;
import com.zchouafia.cards.domain.ports.application.CardsService;
import com.zchouafia.cards.helpers.TestHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class CardsControllerTest {

    private MockMvc mockMvc;

    @Mock
    private CardsService cardsService;

    @BeforeEach
    public void setup() {
        CardsController sut = new CardsController(cardsService);
        this.mockMvc = MockMvcBuilders.standaloneSetup(sut).build();
    }

    @Test
    public void should_return_cards() throws Exception {
        //given
        List<Card> cards = TestHelper.readListFromJson("cards.json", Card.class);
        when(cardsService.getCards(10)).thenReturn(cards);

        //when
        //then
        mockMvc.perform(get("/api/v1/cards").queryParam("size", "10"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty())
                .andExpect(MockMvcResultMatchers.content().json("[{\"color\":\"Coeur\",\"number\":11},{\"color\":\"Pique\",\"number\":3},{\"color\":\"Pique\",\"number\":5},{\"color\":\"Pique\",\"number\":12},{\"color\":\"Coeur\",\"number\":13},{\"color\":\"Coeur\",\"number\":3},{\"color\":\"Coeur\",\"number\":7},{\"color\":\"Trefle\",\"number\":6},{\"color\":\"Carreaux\",\"number\":2},{\"color\":\"Carreaux\",\"number\":6}]"));

        verify(cardsService, times(1)).getCards(10);
    }

    @Test
    public void should_return_bad_request() throws Exception {
        //given
        //when
        //then
        mockMvc.perform(get("/api/v1/cards"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());

        verify(cardsService, times(0)).getCards(10);
    }

    @Test
    public void should_return_sorted_cards() throws Exception {
        //given
        List<Card> cards = TestHelper.readListFromJson("cards.json", Card.class);
        List<Card> sortedCards = TestHelper.readListFromJson("sortedCards.json", Card.class);
        CardsRequest cardsRequest = new CardsRequest();
        cardsRequest.setCards(cards);
        when(cardsService.sortCards(cards)).thenReturn(sortedCards);

        //when
        //then
        mockMvc.perform(post("/api/v1/cards")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TestHelper.toJsonFormat(cardsRequest)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty())
                .andExpect(MockMvcResultMatchers.content().json("[{\"color\":\"Carreaux\",\"number\":2},{\"color\":\"Carreaux\",\"number\":6},{\"color\":\"Coeur\",\"number\":3},{\"color\":\"Coeur\",\"number\":7},{\"color\":\"Coeur\",\"number\":11},{\"color\":\"Coeur\",\"number\":13},{\"color\":\"Pique\",\"number\":3},{\"color\":\"Pique\",\"number\":5},{\"color\":\"Pique\",\"number\":12},{\"color\":\"Trefle\",\"number\":6}]"));

        verify(cardsService, times(1)).sortCards(any());
    }

    @Test
    public void should_return_empty_list() throws Exception {
        //given
        CardsRequest cardsRequest = new CardsRequest();
        cardsRequest.setCards(Collections.emptyList());
        when(cardsService.sortCards(Collections.emptyList())).thenReturn(Collections.emptyList());

        //when
        //then
        mockMvc.perform(post("/api/v1/cards")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TestHelper.toJsonFormat(cardsRequest)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isEmpty());

        verify(cardsService, times(1)).sortCards(any());
    }

    @Test
    public void should_return_bad_request_for_sorted_cards() throws Exception {
        //given
        CardsRequest cardsRequest = new CardsRequest();

        //when
        //then
        mockMvc.perform(post("/api/v1/cards")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TestHelper.toJsonFormat(cardsRequest)))
                .andDo(print())
                .andExpect(status().isBadRequest());

        verify(cardsService, times(0)).sortCards(any());
    }
}
