package com.zchouafia.cards.helpers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class TestHelper {

    public static <T> T readFromJson(String fileName, Class<T> clazz) {
        try(InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName)) {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(in, clazz);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> List<T> readListFromJson(String fileName, Class<T> clazz) {
        CollectionType typeReference =
                TypeFactory.defaultInstance().constructCollectionType(List.class, clazz);
        try(InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName)) {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(in, typeReference);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String toJsonFormat(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }

}
