package com.zchouafia.cards.domain.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CardColorTest {

    @Test
    public void should_return_card_color_from_value() {
        //given
        CardColor color = CardColor.H;

        //when
        CardColor cardColor = CardColor.ofValue("Coeur");

        //then
        Assertions.assertSame(color, cardColor);
    }

    @Test
    public void should_return_null_card_color() {
        //given
        CardColor cardColor = CardColor.ofValue("test");

        //when
        //then
        Assertions.assertNull(cardColor);
    }
}
