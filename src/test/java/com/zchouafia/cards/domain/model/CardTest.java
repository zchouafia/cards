package com.zchouafia.cards.domain.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CardTest {
    @Test
    public void should_check_equals() {
        // given
        Card card1 = new Card(CardColor.D, CardNumber.A);
        Card card2 = new Card(CardColor.D, CardNumber.A);

        //when
        //then
        Assertions.assertNotSame(card1, card2);
        Assertions.assertEquals(card1, card2);
    }

    @Test
    public void should_check_not_equals() {
        // given
        Card card1 = new Card(CardColor.D, CardNumber.A);
        Card card2 = new Card(CardColor.C, CardNumber.A);

        //when
        //then
        Assertions.assertNotSame(card1, card2);
        Assertions.assertNotEquals(card1, card2);
    }
}
