package com.zchouafia.cards.domain.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CardNumberTest {

    @Test
    public void should_return_card_number_from_value() {
        //given
        CardNumber number = CardNumber.K;

        //when
        CardNumber cardNumber = CardNumber.ofValue(13);

        //then
        Assertions.assertSame(number, cardNumber);
    }

    @Test
    public void should_return_null_card_number_from_value() {
        //given
        CardNumber cardNumber = CardNumber.ofValue(0);

        //when
        //then
        Assertions.assertNull(cardNumber);
    }
}
