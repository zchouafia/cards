package com.zchouafia.cards.domain.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ApiErrorTest {

    @Test
    public void should_set_the_message() {
        //given
        String message = "Error";
        ApiError apiError = new ApiError(message);

        //when
        //then
        assertEquals(message, apiError.message());
    }
}
