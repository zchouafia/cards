package com.zchouafia.cards.domain.service;

import com.zchouafia.cards.domain.model.Card;
import com.zchouafia.cards.domain.ports.infrastructure.CardsRepository;
import com.zchouafia.cards.helpers.TestHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest()
public class CardsServiceImplTest {

    private CardsServiceImpl sut;

    @Value("#{'${cards.colors.order}'.split(';')}")
    private List<String> colors;

    @Value("#{'${cards.numbers.order}'.split(';')}")
    private List<Integer> numbers;

    @Mock
    private CardsRepository cardsRepository;

    @BeforeEach
    public void setup() {
        sut = new CardsServiceImpl(cardsRepository);
    }

    @Test
    public void should_return_cards() {
        //given
        List<Card> cardList = TestHelper.readListFromJson("cards.json", Card.class);
        when(cardsRepository.getCards(10)).thenReturn(cardList);

        //when
        List<Card> cards = sut.getCards(10);

        //then
        assertNotNull(cards);
        assertEquals(cardList.size(), cards.size());
    }

    @Test
    public void should_return_cards_sorted() {
        //given
        List<Card> cardList = TestHelper.readListFromJson("cards.json", Card.class);
        List<Card> sortedCards = TestHelper.readListFromJson("sortedCards.json", Card.class);
        when(cardsRepository.getColorsOrder()).thenReturn(colors);
        when(cardsRepository.getNumbersOrder()).thenReturn(numbers);

        //when
        List<Card> cards = sut.sortCards(cardList);

        //then
        assertNotNull(cards);
        assertEquals(cardList.size(), cards.size());
        assertEquals(sortedCards, cards);
    }

    @Test
    public void should_return_empty_cards_sorted() {
        //given
        List<Card> cardList = TestHelper.readListFromJson("cards.json", Card.class);
        when(cardsRepository.getColorsOrder()).thenReturn(null);
        when(cardsRepository.getNumbersOrder()).thenReturn(null);

        //when
        List<Card> cards = sut.sortCards(cardList);

        //then
        assertNotNull(cards);
        assertEquals(0, cards.size());
    }
}
